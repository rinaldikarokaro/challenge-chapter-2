const readline = require("readline-sync");
const penilaian = require("./penilaian");

let x;
let arr = [];

let nilai = new penilaian.KKM();

for (let i = 1; i >= 0; i++) {
  x = readline.question(`Masukkan nilai ke-${i} >`);
  if (x == "q" || x == "Q") {
    console.log("Data yang di input adalah ", arr);
    let sort = sorting(arr);
    console.log("Hasil pengurutan data adalah ", sort);
    console.log("Nilai Terendah dari data adalah ", sort[0]);
    let length = sort.length;
    console.log("Nilai Tertinggi dari data adalah ", sort[length - 1]);
    console.log(nilai.penilaian(arr));
    console.log(avg(sort));
    break;
  } else {
    arr.push(x);
  }
}

function sorting(arr) {
  let result = arr.sort(function (a, b) {
    return a - b;
  });
  return result;
}

function avg(arr) {
  let result = 0;
  let len = parseInt(arr.length);
  for (i = 0; i < arr.length; i++) {
    result += parseInt(arr[i]);
  }
  result = result / len;
  return `Nilai rata-rata dari seluruh data adalah ${result}`;
}
