class Nilai {
  constructor(lulus, tdklulus) {
    this.lulus = lulus;
    this.tdklulus = tdklulus;
  }
  total(lulus, tdkLulus) {
    return `Jumlah Siswa yang lulus = ${lulus} dan Jumlah siswa yang tidak lulus = ${tdkLulus} dengan total seluruh siswa adalah ${lulus + tdkLulus}`;
  }
}

class KKM extends Nilai {
  constructor(lulus, tdkLulus) {
    super(lulus, tdkLulus);
  }

  penilaian(arr) {
    let totalTlulus = 0;
    let totalLulus = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] < 75) {
        totalTlulus += 1;
        console.log(`Data ke-${i + 1} dengan nilai ${arr[i]} tidak lulus`);
      } else if (arr[i] >= 75) {
        totalLulus += 1;
        console.log(`Data ke-${i + 1} dengan nilai ${arr[i]} sudah lulus `);
      }
    }
    return super.total(totalLulus, totalTlulus);
  }
}
module.exports = { KKM, Nilai };

// module.exports = {
//     kkm : function(arr){
//         const newNilai = new Nilai()
//         let lulus = 0
//         let tdkLulus = 0
//         for(let i = 0;i<arr.length;i++){
//             if(arr[i] < 75){
//                 tdkLulus += 1
//                 console.log(`Data ke-${i+1} dengan nilai ${arr[i]} tidak lulus`)
//             }
//             else if(arr[i] >= 75){
//                 lulus += 1
//                 console.log(`Data ke-${i+1} dengan nilai ${arr[i]} sudah lulus `)
//             }
//         }
//         return newNilai.penilaian(lulus,tdkLulus)
//     }
// }
